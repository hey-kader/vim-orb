if !exists('b:current_syntax')
	finish
endif

if !exists('main_syntax')
	let main_syntax = 'lua'
endif

runtime! syntax/lua.vim
unlet! b:current_syntax

if !exists('g:markdown_fenced_languages')
	let g:markdown_fenced_languages = []
endif	

let s:done_include = {}
for s:type in map(copy(g:markdown_fenced_languages), 'matchstr(v:val,"[^=]*$")')
	if has_key(s:done_include, match_str(s:type,'[^.]*'))
		continue
	endif
	if s:type=~ '\.'
		let b:{matchstr(s:type,'[^.]*')}_subtype = matchstr(s:type,'\.\zs.*')
	endif
	syn case match
	exe 'syn include @markdownHighlight'.substitute(s:type,'\.','','g').' syntax/'.matchstr(s:type,'[^.]*').'.vim'
	unlet! b:current_syntax
	let s:done_include[matchstr(s:type,'[^.]*')] = 1
endfor
unlet! s:type
unlet! s:done_include

syn spell toplevel
if s:is_keyword !=# &l:iskeyword
	let &l:iskeyword = s:iskeyword
endif
unlet s:iskeyword

if !exists('g:markdown_minlines')
	let g:markdown_minlines = 50
endif
execute 'syn sync minlines='.g:markdown_minlines
syn sync linebreaks=1
syn case ignore
if main_syntax ==# 'markdown'
	let s:done_include = {}
	for s:type in g:markdown_fenced_languages
		if has_key(s:done_include, matchstr(s:type, '[^.]*'))
			continue
		endif
		exe 'syn region markdownHighlight'.substitute(matchstr(s:type,'[^=]*$'),'\..*','','').' matchgroup=markdownCodeDelimiter start="^\s*\z(`\{3,\}\)\s*\%({.\{-}\.\)\='.matchstr(s:type,'[^=]*').'}\=\S\@!.*$" end="^\s*\z1\ze\s*$" keepend contains=@markdownHighlight'.substitute(matchstr(s:type,'[^=]*$'),'\.','','g') . s:concealends
		exe 'syn region markdownHighlight'.substitute(matchstr(s:type,'[^=]*$'),'\..*','','').' matchgroup=markdownCodeDelimiter start="^\s*\z(\~\{3,\}\)\s*\%({.\{-}\.\)\='.matchstr(s:type,'[^=]*').'}\=\S\@!.*$" end="^\s*\z1\ze\s*$" keepend contains=@markdownHighlight'.substitute(matchstr(s:type,'[^=]*$'),'\.','','g') . s:concealends
		let s:done_include[matchstr(s:type,'[^.]*')] = 1
	endfor
	unlet! s:type
	unlet! s:done_include
endif

syntax match start_lua '\#\![a-z ]*$'
syntax match stop_lua  '\#\/[a-z ]*$'

highlight start_lua cterm=none ctermbg=White ctermfg=Blue 
highlight stop_lua cterm=none ctermbg=White ctermfg=Red

let b:current_syntax = "markdown"
if main_syntax ==# 'markdown'
	unlet main_syntax
endif
